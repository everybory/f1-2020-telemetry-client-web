// (A) RAW JSON DATA
let counter = 0;
let oldCounter = 0;
let date = new Date();
let oldHash = '';
let team_colors = {}
let API_URL = "http://" + API_SERVER + ":" + API_PORT;
let playStatus = true;
let playersData = [];
let sessionData = [];
let oldPlayerView = null;
let newPlayerView = null;
let oldSpectatingSource = null;
let newSpectatingSource = null;
let auto = true;
let globalSourceSelected = null;
let lastDateFinalClassificationPacket = null;
let obsConnectionStatus = null;
setInterval(function () { calcFps(counter, new Date()) }, 1000);


function parseData(jsonData) {
    counter++;
    let parsed = null;

    try {
        parsed = JSON.parse(jsonData);
    } catch (error) {
        console.error(error);
        console.log(jsonData);
    }

    playersData = parsed['players'];
    team_colors = parsed['team_colors'];
    sessionData = parsed['session'];
    setObsConnectionStatus(parsed['obs_connected']);

    lastDateFinalClassificationPacket = parsed['last_date_final_classification_packet'];

    newSpectatingSource = $('tr[spectating=1]').find('td').eq(7).children('select').children("option:selected").text().trim();
    newPlayerView = playersData[sessionData['spectatorCarIndex']]['sourceName'];

    if (auto && ((newSpectatingSource != oldSpectatingSource) || (newPlayerView != oldPlayerView))){
        if(newPlayerView != '' || newSpectatingSource == 'select'){
            setOBSSourceView(newPlayerView);
            oldPlayerView = newPlayerView;
            oldSpectatingSource = newSpectatingSource;
        }
    }

    drawHeader(sessionData);
    drawHeaderLastPacketFinalClassification(lastDateFinalClassificationPacket);

    let newHash = md5(JSON.stringify(playersData));
    if (oldHash != newHash) {
        if (playersData.length > 0 && playersData[0].carPosition != 0) {
            drawTable(playersData);
        }
    }
    oldHash = newHash;
}

function drawTable(playersData) {
    $("#leadertable tr").remove();
    let theTable = $("#leadertable");

    let row = null;
    let rows = null;
    let listTh = null;
    let theKeyCell = null;
    let theValueCell = null;

    playersData.sort(function (a, b) {
        return a.carPosition - b.carPosition;
    });

    // INSERT HEADER NAMES
    // row = '<tr>';
    // for (let key in playersData[0]) {
    //     listTh += '<th></th>';
    // }
    // row += listTh;
    // row += '</tr>';

    rows += row;

    playersData.forEach((element, index) => {
        let listTd = null;
        let teamColor = null;

        for (let key in playersData[0]) {
            let data = element['raceNumber'] + ',' + element['name'] + ',' + element['teamDescription'] + ',' + element['carPosition'] + ',' + element['trackIndividualId'];
            if (key == 'carPosition') {
                if(element['carPosition'] == 0){
                    break;
                }
                listTd += '<td> <div class="box-position">' + element[key] + '</div></td>';
            } else if (key == 'name') {
                listTd += '<td style="cursor: pointer" data="'+data+'" onclick="popup(this)">' + element['name'] + '</td>';
            } else if (key == 'trackIndividualId') {
                if (element[key] == 0) {
                    listTd += '<td> - </td>';
                } else {
                    listTd += '<td> + </td>';
                }
            } else if (key == 'sourceName') {
                if (element['sourceName'] != '') {
                    listTd += '<td> <select class="select-sources custom-select custom-select-sm" onchange="setSource(this)" data="' + data + '"> ' + getOBSOptionSources(element["sourceName"]) + ' </select> </td>';
                } else {
                    listTd += '<td> <select class="select-sources custom-select custom-select-sm" onchange="setSource(this)" data="' + data + '"> ' + getOBSOptionSources('select') + ' </select> </td>';
                }
            }
            else if (key == 'spectating') {

            } else {
                listTd += '<td>' + element[key] + '</td>';
            }
            if (key == 'teamDescription') {
                teamColor = getColorFromTeamName(element[key]);
            }
        }

        row = '<tr team-color="' + teamColor + '" spectating="'+element['spectating']+'" style="">';
        row += listTd;
        row += '</tr>';
        rows += row;
    });

    theTable.append(rows);
    insertColumnColor(theTable);
    insertColumnAutoOn(theTable);

}

function insertColumnColor(table) {
    $(table).find('tr').each(function () {
        let teamColor = $(this).attr("team-color");
        let spectating = $(this).attr('spectating');
        if(spectating == 1){
            $(this).css('background-color', 'rgba(0,125,255,.3)');
        }
        $(this).find('th').eq(0).after('<th></th>');
        $(this).find('td').eq(0).after('<td><div style="background-color: ' + teamColor + ';" class="box-color"></div></td>');
    });
}

function insertColumnAutoOn(table) {
    let sourceName = '';
    $(table).find('tr').each(function () {
        sourceName = $(this).find('td').eq(7).children('select').children("option:selected").text();
        $(this).find('td').eq(-1).after('<td><input onclick="setManual(this)" type="radio" name="auto-on" class="auto-on" sn-auto="'+sourceName+'" value="on"></td>');
    });
    if (globalSourceSelected != null && globalSourceSelected != ''){
        let option = $('input[sn-auto="'+globalSourceSelected+'"]')[0];
        $(option).prop("checked", true);
    }
}

function getOBSOptionSources(selected){
    let options = '<option value="select">select</option>';
    if (currentScene != null){
        $(currentScene.sources).each(function(index, value ) {
            if (selected == value){
                options += '<option value="'+value+'" selected>'+value+'</option>';
            }else{
                options += '<option value="'+value+'">'+value+'</option>';
            }

        });
    }
    return options;
}

function popup(t) {
    $('#playerNameMessage').hide();
    $('#playerNameMessage').removeClass('alert-success');
    $('#playerNameMessage').removeClass('alert-danger');
    let allData = t.getAttribute("data").split(',');
    $('#raceNumber').val(allData[0]);
    $('#driverName').val(allData[1]);
    $('#teamName').val(allData[2]);
    $('#position').val(allData[3]);
    if (allData[4] > 0) {
        $('#selectTrackIndividual').val(allData[4]);
        $('#selectTrackIndividual').trigger("chosen:updated");
    } else {
        $('#selectTrackIndividual').val('');
        $('#selectTrackIndividual').trigger("chosen:updated");
    }
    $('#playerModal').modal();
}

function drawHeader(sessionData) {
    let div = $('#session');
    let divTemp = '';
    for (let key in sessionData) {
        if (key == 'sessionTypeDescription') {
            divTemp += '<div class="col-sm-12"><h3>SESSION ' + sessionData[key] + '</h3></div>';
        }
        if (key == 'sessionTimeLeft') {
            divTemp += '<div class="col-sm-12"><h5>TIME LEFT ' + sessionData[key] + '</h5></div>';
        }
    }
    if (sessionData != []){
        $('#selectCategories').val(sessionData['category']);
    }
    if (sessionData != [] && !jQuery.isEmptyObject(sessionData['currentScene'])){
        $('#obsScenes').val(sessionData['currentScene']);
    }
    if (sessionData != [] && !jQuery.isEmptyObject(sessionData['dynamicSource'])){
        if($('#dynamic-source').val() != '' && $('#dynamic-source').val() == 'select') {
           $('#obsScenes').change();
           $("#dynamic-source").val(sessionData['dynamicSource']);
        }
    }
    div.html(divTemp);
}

function drawHeaderLastPacketFinalClassification(lastDate) {
    if (lastDate != ''){
        $('#lastDateFinalClassificationBox').show();
        $('#lastDateFinalClassificationText').html(lastDate);
    }
}

function calcFps(counter) {
    fps = counter - oldCounter;
    $('#fps').html('FPS: ' + fps);
    oldCounter = counter;
}

function getColorFromTeamName(team_name) {
    let color = '';
    color = team_colors[team_name];
    if (color === undefined) {
        color = '#FFFFFF'
    }
    return color;
}

function getContrastColor(color) {
    let rgb = [];
    let defaultColor = 'FFFFFF';

    if (typeof color === 'string' && color.startsWith('#')) {
        color = color.substring(1);
    } else {
        color = defaultColor;
    }

    for (let i = 0; i < color.length; i += 2) {
        rgb.push(color[i] + '' + color[i]);
    }

    for (let i = 0; i < rgb.length; i++) {
        rgb[i] = parseInt(rgb[i], 16);
    }

    const brightness = Math.round(((parseInt(rgb[0]) * 299) +
        (parseInt(rgb[1]) * 587) +
        (parseInt(rgb[2]) * 114)) / 1000);
    const textColour = (brightness > 125) ? 'black' : 'white';

    return textColour;
}

function getTrackIndividualData() {
    return $.ajax({
        type: "GET",
        url: API_URL + "/getTrackIndividual",
        async: false
    }).responseText;
}

function updatePlayerName() {
    let playerName = $('#selectTrackIndividual option:selected').attr('player-name');
    if (playerName !== undefined && playerName != '') {
        playerName = playerName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        playerName = capitalize(playerName);
        $('#driverName').val(playerName.trim());
    }

}

function togglePlayButton(obj) {
    playStatus = !playStatus;
    if (playStatus) {
        $(obj).text("Pause");
    } else {
        $(obj).text("Play");
    }
}

function confirmToUpdatePlayerName() {
    let newPlayerName = $('#selectTrackIndividual option:selected').attr('player-name');
    let actualPlayerName = $('#driverName').val();
    let normalizedNewPlayerName = newPlayerName;
    let normalizedActualPlayerName = actualPlayerName;
    try{
        normalizedNewPlayerName = newPlayerName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }catch (e) {
        console.log(e)
    }
    try{
        normalizedActualPlayerName = actualPlayerName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");    }catch (e) {
        console.log(e)
    }

    if (newPlayerName != undefined && newPlayerName != '' && normalizedNewPlayerName != normalizedActualPlayerName) {
        let confirmed = confirm("El nomrbe del jugador actual es distinto del nombre de joomla, desea actualizarlo?");
        if (confirmed) {
            updatePlayerName();
        }
    }
}

function clearTrackIndividual() {
    let confirmed = confirm("Desea borrar la relacion de Jugadores/Joomla?");
    if (confirmed) {
        return $.ajax({
            type: "POST",
            url: API_URL + "/clearTrackIndividual",
            async: true
        }).responseText;
    }
}

function downloadFileDriverNames() {
    window.open(API_URL + "/downloadFileDriverNames", '_blank');
}

function getWeeklyFilesList(){
    return $.ajax({
        type: "GET",
        url: API_URL + "/getListWeeklyFilesDriverName",
        async: false
    }).responseText;    
}

function getCategoryList(){
    return $.ajax({
        type: "GET",
        url: API_URL + "/getCategories",
        async: false
    }).responseText;    
}

function getEventList(){
    return $.ajax({
        type: "GET",
        url: API_URL + "/getEvents",
        async: false
    }).responseText;
}

function getEventRace(){
    return $.ajax({
        type: "GET",
        url: API_URL + "/getEventRace",
        async: false
    }).responseText;
}

function getEventQuali(){
    return $.ajax({
        type: "GET",
        url: API_URL + "/getEventQuali",
        async: false
    }).responseText;
}


function loadSelectWeeklyFiles(){
    let jsonData = JSON.parse(getWeeklyFilesList());
    $.each(jsonData, function (i, file) {
        let option = '<option value="' + file + '" >' + file + '</option>';
        $("#selectWeeklyFile").append(option);
    });
}

function loadSelectTrackIndividual(){
    let jsonData = JSON.parse(getTrackIndividualData());
    $.each(jsonData, function (i, trackIndividual) {
        let option = '<option value="' + trackIndividual.id + '" nick-name="'+trackIndividual.nickname+'" player-name="' + trackIndividual.first_name + ' ' + trackIndividual.last_name + '">' + trackIndividual.nickname + ' - ' + trackIndividual.first_name + ' ' + trackIndividual.last_name + '</option>';
        $("#selectTrackIndividual").append(option);
    });
}

function loadSelectCategories(){
    let jsonData = JSON.parse(getCategoryList());
    $.each(jsonData, function (i, file) {
        let option = '<option value="' + file + '" >' + file + '</option>';
        $("#selectCategories").append(option);
    });
}

function loadSelectEvents(){
    let jsonData = JSON.parse(getEventList());
    $.each(jsonData, function (i, row) {
        let option = '<option value="'+ row.eventId + '">' + row.category2 +" - "+ row.eventTypeName + " - " + row.trackName + '</option>';
        $("#selectEventsRace").append(option);
        $("#selectEventsQuali").append(option);
    });

    let selectedEventRace = JSON.parse(getEventRace()).eventId;
    let selectedEventQuali = JSON.parse(getEventQuali()).eventId;
    $("#selectEventsRace option[value='"+selectedEventRace+"']").prop('selected', true);
    $("#selectEventsQuali option[value='"+selectedEventQuali+"']").prop('selected', true);

}

function setCurrentFileDriverName(){
    let fileName = $('#selectWeeklyFile option:selected').val();
    let data = {
        'fileName': fileName
    }
    let response = $.ajax({
        type: "GET",
        url: API_URL + "/setCurrentFileDriverName",
        data: data,
        async: false
    }).responseText;
    response = JSON.parse(response);
    $('#selectWeeklyFileStatus').text(response['status']);
    setTimeout(function(){ $('#selectWeeklyFileStatus').text(''); }, 1000);
    $('#selectWeeklyFile').prop('selectedIndex',0);
}

function setCategory(obj){
    let category =  $(obj).children("option:selected").val();
    let data = {'category': category}
    let response = $.ajax({
        type: "GET",
        url: API_URL + "/setCategory",
        data: data,
        async: false
    }).responseText;
    setStatus("success","selectCategoryStatus" );

}

function setEventRace(obj){
    let selectedEvent =  $(obj).children("option:selected").val();
    let jsonData = JSON.parse(getEventList());
    let data = {}
    $(jsonData).each(function(i, row){
        if(row.eventId == selectedEvent){
            data = JSON.stringify(row);
            return false;
        }
    });
    let response = $.ajax({
        type: "GET",
        url: API_URL + "/setEventRace",
        data: {"event" : data},
        async: false
    }).responseText;
    setStatus("success","selectEventRaceStatus" );
}

function setEventQuali(obj){
    let selectedEvent =  $(obj).children("option:selected").val();
    let jsonData = JSON.parse(getEventList());
    let data = {}
    $(jsonData).each(function(i, row){
        if(row.eventId == selectedEvent){
            data = JSON.stringify(row);
            return false;
        }
    });
    let response = $.ajax({
        type: "GET",
        url: API_URL + "/setEventQuali",
        data: {"event" : data},
        async: false
    }).responseText;
    setStatus("success","selectEventQualiStatus" );
}

function setManual(el){
    auto = false;
    $('#auto-check').prop('checked', auto);
    let sourceName = $(el).attr('sn-auto');
    globalSourceSelected = sourceName;
    if(sourceName != ''){
        setOBSSourceView(sourceName);
    }
}

function changeAuto(el){
    auto = $(el).prop('checked');
    if(auto){
        let options = $("input[name='auto-on']:checked");
        $(options).prop('checked', false);
    }
}

function loadOptionDynamicSources(){
    $('#dynamic-source').find('option').remove().end().append();
    $('#dynamic-source').append(getOBSOptionSources('select'));
}

function setCurrentScene(el){
    let sceneName = $(el).children("option:selected").text();
    return $.ajax({
        type: "GET",
        url: API_URL + "/setOBSScene",
        data: {sceneName: sceneName},
        async: true
    }).responseText;
}

function createSource(data){
    $.ajax({
        method: "GET",
        url: API_URL + "/createSource",
        data: data,
        crossDomain: true,
    }).done(function (response) {
        if (response['status'] == 'success') {
            snack({message: 'Source has been created', status: 'success'});
        }
    }).fail(function() {
        snack({message: 'Error creating source', status: 'danger'});
    });
}

function capitalize(str){
    return str.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase())));
}

$('#btnSavePlayerName').click(function () {

    confirmToUpdatePlayerName();

    let driverName = $('#driverName').val();

    try {
        driverName.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    } catch (error) {
        console.error(error);
    }

    let msg = $('#playerNameMessage');
    msg.removeClass('alert-success');
    msg.removeClass('alert-danger');
    let data = {
        'id': $('#raceNumber').val(),
        'name': $('#driverName').val(),
        'track_individual_id': $('#selectTrackIndividual').val(),
        'nick_name': $('#selectTrackIndividual').find(':selected').attr('nick-name'),
        'connection': getCurrentOBSConnection(),
    }

    data.scene_id = '1';
    createSource(data);
    data.scene_id = '2';
    createSource(data);

    $.ajax({
        method: "GET",
        url: API_URL + "/addDriverName",
        data: data,
        crossDomain: true,
    }).done(function (response) {
        if (response['status'] == 'success') {
            msg.addClass('alert-' + response['status']);
            msg.html(response['status']);
            msg.show();
        } else {
            msg.addClass('alert-danger');
            msg.html(response['status'] + '<br/>' + response['log']);
            msg.show();
        }
    });

    $('#leadertable tr').each(function () {
        if ($(this).children().eq(4).text() == $('#raceNumber').val()) {
            $(this).children().eq(5).html('+');
        }
    })

});

$(function () {
    getWeeklyFilesList();
    loadSelectWeeklyFiles();
    loadSelectCategories();
    loadSelectEvents();
    loadSelectTrackIndividual();

    $('.chosen-select').chosen();
    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
    $('.chosen-container').removeAttr("style");

    $('#auto-check').prop('checked', true);

});

function snack(data){
    SnackBar(data);
}