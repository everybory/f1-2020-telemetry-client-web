function WebSocketTest() {
   var was_connected = false;
   if ("WebSocket" in window) {
      // Let us open a web socket
      var ws = new WebSocket("ws://"+WEBSOCKET_SERVER+":"+WEBSOCKET_PORT+"");

      ws.onopen = function () {
         ws.send("Hello");
      };

      ws.onmessage = function (evt) {
         if(playStatus){
            was_connected = true;
            var received_msg = evt.data;
            parseData(received_msg);
            $('#status').html('Connected');
            $('#status').addClass('alert-success');
            $('#status').removeClass('alert-danger');
            $('#status').removeClass('alert-info');
         }
      };

      ws.onclose = function () {
         if (was_connected) {
            $('#status').html('Connection closed. <br/>Reconnecting...');
            $('#status').addClass('alert-danger');
            $('#status').removeClass('alert-success');
            $('#status').removeClass('alert-info');
         }
         WebSocketTest();
      };
   } else {
      // The browser doesn't support WebSocket
      alert("WebSocket NOT supported by your Browser!");
   }
}


WebSocketTest()