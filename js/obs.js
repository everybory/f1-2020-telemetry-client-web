let scenes = [];

class Scene {
    constructor(sceneName, sources) {
        this.name = sceneName;
        this.sources = sources;
    }
}
let currentScene = null;

$(function () {

    loadSelectOBSConnection();

    $('#obsScenes').change(function(){
        let connection = $('#selectOBSConnection option:selected').text();
        let sceneName = $('#obsScenes option:selected').text();
        let jsonData = JSON.parse(getOBSSources(connection, sceneName));
        currentScene = new Scene(sceneName, jsonData);
        drawTable(playersData);
        loadOptionDynamicSources();
    });

});


function loadSelectOBSConnection() {
    let jsonData = JSON.parse(getOBSConnectionList());
    let currentConnection = getOBSConnection();
    $("#selectOBSConnection").append('<option>select</option>');
    $.each(jsonData, function (i, data) {
        let option = '<option value="' + data + '" >' + data + '</option>';
        $("#selectOBSConnection").append(option);
    });
    if(currentConnection != ""){
        $("#selectOBSConnection option[value="+currentConnection+"]").attr('selected', true);
    }
    setOBSConnection();
}

function loadSelectOBSScenes() {
    let connection = getCurrentOBSConnection();
    let jsonData = JSON.parse(getOBSScenesList(connection));
    $("#obsScenes").find('option').remove();
    $("#obsScenes").append('<option>scene name</option>');
    $.each(jsonData, function (i, data) {
        let option = '<option value="' + data + '">' + data + '</option>';
        $("#obsScenes").append(option);
    });
}

function loadSelectOBSFinalScene() {
    let connection = getCurrentOBSConnection();
    let jsonData = JSON.parse(getOBSScenesList(connection));
    $("#selectFinalScene").find('option').remove();
    $("#selectFinalScene").append('<option>scene name</option>');
    $.each(jsonData, function (i, data) {
        let option = '<option value="' + data + '">' + data + '</option>';
        $("#selectFinalScene").append(option);
    });
    let currentFinalScene = getFinalScene();
    $('#selectFinalScene option[value='+currentFinalScene+']').attr('selected', 'selected');
}

function getCurrentOBSConnection(){
    return $('#selectOBSConnection option:selected').text();
}

function getCurrentOBSDynamicSource(){
    return $('#dynamic-source option:selected').text();
}

function getCurrentOBSScene(){
    return $('#obsScenes option:selected').text();
}

function forceOBSConnectionAndLoadScenes(){
    forceOBSConnection();
}

function getOBSConnectionList() {
    return $.ajax({
        type: "GET",
        url: API_URL + "/getOBSConnectionList",
        async: false
    }).responseText;
}

function getOBSScenesList(connection) {
    if(getCurrentOBSConnection().trim() != 'select') {
        return $.ajax({
            type: "GET",
            url: API_URL + "/getOBSScenesList",
            data: {connection: connection},
            async: false
        }).responseText;
    }else{
        return false;
    }
}

function getOBSSources(connection, sceneName) {
    return $.ajax({
        type: "GET",
        url: API_URL + "/getOBSSources",
        data:{connection: connection, sceneName: sceneName },
        async: false
    }).responseText;
}

function getOBSConnection() {
    return $.ajax({
        type: "GET",
        url: API_URL + "/getOBSConnection",
        async: false
    }).responseText;
}

function setSource(el){
    let data = $(el).attr('data');
    let raceNumber = data.split(',')[0]
    let sourceName = $(el).children("option:selected").text();
    return $.ajax({
        type: "GET",
        url: API_URL + "/setOBSSources",
        data:{raceNumber: raceNumber, sourceName: sourceName },
        async: true
    }).responseText;
}

function setDynamicSource(el){
    let sourceName = $(el).children("option:selected").text();
    let sceneName = getCurrentOBSScene();
    return $.ajax({
        type: "GET",
        url: API_URL + "/setOBSDynamicSource",
        data:{sourceName: sourceName, sceneName: sceneName},
        async: true
    }).responseText;
}

function getDynamicSource() {
    return $.ajax({
        type: "GET",
        url: API_URL + "/getOBSDynamicSource",
        async: false
    }).responseText;
}

function setOBSConnection(){
    if(getCurrentOBSConnection().trim() != 'select'){
        $.ajax({
            type: "GET",
            url: API_URL + "/setOBSConnection",
            data:{connection: getCurrentOBSConnection()},
            async: true
        }).done(function(data) {
            loadSelectOBSScenes();
            loadSelectOBSFinalScene();
        });
    }
}

function forceOBSConnection(){
    if(getCurrentOBSConnection().trim() != 'select'){
        $.ajax({
            type: "GET",
            url: API_URL + "/forceOBSConnection",
            data:{connection: getCurrentOBSConnection()},
            async: true
        }).done(function(data) {
            //setStatus(data['status'], 'connectOBS');
            loadSelectOBSScenes();
            loadSelectOBSFinalScene();
        });
    }
}

function setOBSSourceView(sourceName){
    if(getCurrentOBSScene().trim() != 'scene name') {
        return $.ajax({
            type: "GET",
            url: API_URL + "/setOBSSourceView",
            data: {
                connection: getCurrentOBSConnection(),
                sceneName: getCurrentOBSScene(),
                sourceName: sourceName,
                dynamicSource: getCurrentOBSDynamicSource()
            },
            async: true
        }).responseText;
    }
}

function setStatus(status, objId){
    let obj = '#'+objId;
    $(obj).removeClass('text-success');
    $(obj).removeClass('text-danger');
    $(obj).show();
    $(obj).text(status);
    setTimeout(function(){ $(obj).hide();}, 3000);
    if(status && status == 'error'){
        $(obj).addClass('text-danger');
    }else{
        $(obj).addClass('text-success');
    }
}

function setStatusPermanent(status, objId, color){
    let obj = '#'+objId;
    $(obj).removeClass('text-success');
    $(obj).removeClass('text-danger');
    $(obj).show();
    $(obj).text(status);
    if(color && color == 'danger'){
        $(obj).addClass('text-'+color);
    }else{
        $(obj).addClass('text-'+color);
    }
}

function saveReplayBuffer(){
    $('#btnSaveReplay').removeClass('bg-danger');
    $('#btnSaveReplay').removeClass('bg-success');
    if(getCurrentOBSConnection().trim() != 'select'){
        $.ajax({
            type: "GET",
            url: API_URL + "/saveReplayBuffer",
            data:{connection: getCurrentOBSConnection()},
            async: true
        }).done(function(data) {
            if(data['status'] == 'error'){
                $('#btnSaveReplay').addClass('bg-danger');
                console.log(data);
            }else{
                $('#btnSaveReplay').addClass('bg-success');
            }
        });
    }
}

function setObsConnectionStatus(connected){
    if(connected == 'true'){
        setStatusPermanent('connected', 'connectOBS', 'success');
    }else{
        setStatusPermanent('not connected', 'connectOBS', 'danger');
    }
}

function getFinalScene() {
    return $.ajax({
        type: "GET",
        url: API_URL + "/getFinalScene",
        async: false
    }).responseText;
}

function setCurrentFinalScene(el){
    let sceneName = $(el).children("option:selected").text();
    $.ajax({
        type: "GET",
        url: API_URL + "/setFinalScene",
        data:{sceneName: sceneName},
        async: true
    }).done(function(data) {
        if(data['status'] == 'success'){
            setStatus('success', 'statusFinalScene');
        }else{
            setStatus('error', 'statusFinalScene');
        }
    });
}

function switchScene(el){
    let sceneName = $('#selectFinalScene option:selected').text();
    $.ajax({
        type: "GET",
        url: API_URL + "/switchScene",
        data:{connection: getCurrentOBSConnection(), sceneName: sceneName},
        async: true
    }).done(function(data) {

    });
}
